<?php

  spl_autoload_register(function($classname) {
  // This is needed to fix linux directory separator (/)
  $classname = str_replace("\\", DIRECTORY_SEPARATOR, $classname);
  echo "Loading the class: " . $classname . PHP_EOL;
  require_once $classname . '.php';
});

$file1 = new library\Files();
$file1->hello();

$db1 = new library\Database();
$db1->hello();

$str = new library\StringHelper();
$str->hello();
echo PHP_EOL;
